var express = require("express");
var bodyParser = require("body-parser");
var routes = require("./routes");

var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.post("/api/todolist/save", routes.save(function(){}));

app.get("/api/todolists", routes.getTodolist(function(){}));

app.use(express.static(__dirname + "/public"));

app.listen(3000, function () {
    console.info("App Server started on port 3000");
});
