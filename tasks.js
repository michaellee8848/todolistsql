var mysql = require("mysql");

var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "12571",
    database: "todolist",
    connectionLimit: 4
});

const INSERTSQL = "insert into todolist (task_name, create_date, due_date, completion) values (?,?,?,?)";
const SELECTSQL = "select task_name, create_date, due_date, completion from todolist";

var Todolist = function (taskName, createDate, dueDate, completion) {
    this.taskName = taskName;
    this.createDate = createDate;
    this.dueDate = dueDate;
    this.completion = completion;
};

Todolist.prototype.save = function (callback) {
    var todolist = this;
    pool.getConnection(function (err, connection) {

        if (err) {
            return callback(err);
        }

        var values = [todolist.taskName, todolist.createDate, todolist.dueDate, todolist.completion];

        connection.query(INSERTSQL, values, function (err, result) {

            connection.release();

            if (err) {
                return callback(err);
            }

            callback(null, result);

        });
    });
};

Todolist.prototype.getAllTodo = function (callback) {
    var todolist = this;
    pool.getConnection(function (err, connection) {

        if (err) {
            return callback(err);
        }
        connection.query(SELECTSQL, [], function (err, results) {

            connection.release();

            if (err) {
                return callback(err);
            }

            callback(null, results);

        });
    });
};

module.exports = Todolist;
