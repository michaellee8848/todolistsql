(function () {
    angular
        .module("TodoApp", [])
        .controller("TodoCtrl", TodoCtrl);

    TodoCtrl.$inject = ["$http", "dbService"];

    function TodoCtrl($http, dbService) {
        var vm = this;
        
        vm.todolist = dbService.getTodolist();
        vm.newTodo = createTodo(vm.todolist.length);
        vm.status = {
            message: "",
            code: 0
        };

        vm.addTodo = function () {
            console.info("Client side add todo")
            dbService.save(vm.newTodo)
                .then(function (result) {
                    vm.status.message = "Todo List updated successfully";
                    vm.status.code = 202;
                    console.log(result);
                })
                .catch(function (err) {
                    console.log(err);
                    vm.status.message = "An error occurred.";
                    vm.status.code = 400;
                })
        }
    }
    function createTodo(lenTodos) {
        console.log(lenTodos);
        console.log(".....");
        return ({id: lenTodos + 1, task: '', dueDate: '', priority: ''});
    }
})();