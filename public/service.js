(function () {
    angular.module("TodoApp")
        .service("dbService", dbService);

    dbService.$inject = ["$http", "$q"];

    function dbService($http, $q) {
        var service = this;

        service.getTodo = function () {
            var todolist = {};
            todolist.task = "";
            todolist.duedate = "";
            todolist.priority = "";
            todolist.createdate = "";
            todolist.completion = "";
            return todolist;
        };
        
        service.getTodolist = function() {
            var defer = $q.defer();

            $http.get("/api/todolist/save").then(function (result) {
                defer.resolve(result);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        }
        
        service.save = function (todo, callback) {
            var defer = $q.defer();

            $http.post("/api/todolist/save", todo).then(function (result) {
                defer.resolve(result);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };
    }
})();
